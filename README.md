# ad-patcher-data

This repository has files that are used by [ad-patcher](https://gitlab.com/arbetsformedlingen/joblinks/ad-patcher) for overwriting data in specific ads (identified by the ad's ID)

In some cases, automatic detection of occupation or geographical places goes wrong, this is a way to handle that if it is discovered.
The split into code (ad-patcher) and corrections (ad-patcher-data, this repo) is for two major reasons:
- No new deployment of code is needed.
- Changes are version controlled.


## Usage
The following information is required:
- The ad id
- The attribute(s) that should be overwritten as separate objects in the `"change_list"`attribute. Usually only one, but could be several.
- The new value(s)
- 
Optional:
- Date when added
- A comment why the change is needed
- A reference to the incident in ServiceNow, if applicable

Example:
```json
{
  "12345 - this is where you put the ad id": {
    "comment": "Comments are ignored, the relevant json key is 'change_list'",
    "INC": "the number from ServiceNow, if applicable",
    "Date": "2023-03-09 or whenever you enter the patch information",
    "Change list information": "it's a list of one or more json objects which will be written to the ad",
    "change_list": [
      {
        "key": "workplace_addresses",
        "value": {
          "municipality_concept_id": "???",
          "municipality": "Ekeby",
          "region_concept_id": "???",
          "region": "Skåne län",
          "country_concept_id": "i46j_HmG_v64",
          "country": "Sverige"
        }
      }
    ]
  }
}
```

The `ad-patcher` processor will pick up the file specified by its environment variable `PATCH_FILE_URL` at runtime. 
By default, it's the file `patch_data.json` from this GitLab repo´s `main`branch.

From [ad-patcher:settings.py](https://gitlab.com/arbetsformedlingen/joblinks/ad-patcher/-/blob/main/settings.py):
```
# Link to a json file with changes
PATCH_FILE_URL = os.getenv("PATCH_FILE_URL",
"https://gitlab.com/arbetsformedlingen/joblinks/ad-patcher-data/-/raw/main/patch_data.json")
``` 
